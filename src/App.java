import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        Scanner kb = new Scanner(System.in);
        int value;
        Tree theTree =new Tree();

        theTree.insert(50, 1.5);
        theTree.insert(25, 1.2);
        theTree.insert(75, 1.7);
        theTree.insert(12, 1.5);
        theTree.insert(37, 1.2);
        theTree.insert(43, 1.7);
        theTree.insert(30, 1.5);
        theTree.insert(33, 1.2);
        theTree.insert(87, 1.7);
        theTree.insert(93, 1.5);
        theTree.insert(97, 1.5);

        while (true) {
            System.out.println("Enter first lettor of show. ");
            System.out.println("insert, find, delete, or tranverse:");
            int choice = getChar();
            switch (choice) {
                case 's':
                    theTree.displayTree();
                    break;
                case 'i':
                    System.out.println("Enter value to insert: ");
                    value = kb.nextInt();
                    theTree.insert(value, value + 0.9);
                    break;
                case 'f':
                    System.out.println("Enter value to find: ");
                    value = kb.nextInt();
                    Node found = theTree.find(value);
                    if( found != null) {
                        System.out.println("Found: ");
                        found.displayNode();
                        System.out.println("\n");
                    } else {
                        System.out.println("Could not find ");
                        System.out.println(value + '\n');
                    }
                    break;
                case 'd':
                    System.out.println("Enter value to delete: ");
                    value = kb.nextInt();
                    boolean didDelete = theTree.delete(value);
                    if(didDelete) System.out.println("Deleted " + value + '\n');
                    else  System.out.println("Could not delete " + value + '\n');
                    break;
                case 't':
                    System.out.println("Enter type 1, 2 or 3: ");
                    value = kb.nextInt();
                    theTree.tranverse(value);
                    break;
                default:
                    System.out.println("Invalid entry\n");
            }
        }
    }

    private static char getChar() throws IOException{
        String S = getString();
        String s = S.toLowerCase();
        return s.charAt(0);
    }

    private static String getString() {
        Scanner kb = new Scanner(System.in);
        String s = kb.next();
        return s;
    }
}
